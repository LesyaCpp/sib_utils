# Add all targets to the build-tree export set
export(TARGETS ${LIBNAME}
  NAMESPACE sib_utils::
  FILE "${PROJECT_BINARY_DIR}/${LIBNAME}Targets.cmake")

# Export the package for use from the build-tree
# (this registers the build-tree with a global CMake-registry)
export(PACKAGE ${LIBNAME})

# Create the ${LIBNAME}Config.cmake and ${LIBNAME}Version files
file(RELATIVE_PATH REL_INCLUDE_DIR "${INSTALL_CMAKE_DIR}"
   "${INSTALL_INCLUDE_DIR}")

# ... for the build tree
set(CONF_INCLUDE_DIRS "${PROJECT_SOURCE_DIR}" "${PROJECT_BINARY_DIR}")
configure_file(Common/Config.cmake.in
  "${PROJECT_BINARY_DIR}/${LIBNAME}Config.cmake" @ONLY)

# ... for the install tree
set(CONF_INCLUDE_DIRS "\${${LIBNAME}_CMAKE_DIR}/${REL_INCLUDE_DIR}")
set(PKG_VERSION ${${LIBNAME}_VERSION})
configure_file(Common/Config.cmake.in
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${LIBNAME}Config.cmake" @ONLY)
unset(${PKG_VERSION})

# ... for both
configure_file(Common/ConfigVersion.cmake.in
  "${PROJECT_BINARY_DIR}/${LIBNAME}ConfigVersion.cmake" @ONLY)

# Install the ${LIBNAME}Config.cmake and ${LIBNAME}ConfigVersion.cmake
install(FILES
  "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/${LIBNAME}Config.cmake"
  "${PROJECT_BINARY_DIR}/${LIBNAME}ConfigVersion.cmake"
  DESTINATION "${INSTALL_CMAKE_DIR}" COMPONENT dev)

install(TARGETS ${LIBNAME}
    DESTINATION lib)

install(EXPORT ${LIBNAME}Targets
  NAMESPACE sib_utils::
  DESTINATION "${INSTALL_CMAKE_DIR}"
  COMPONENT Development)
