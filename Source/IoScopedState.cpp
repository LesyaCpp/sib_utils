#include "IoScopedState.hpp"
#include <ios>
using namespace sib_utils;

namespace  {
  bool openIO(QIODevice &io, QIODevice::OpenMode mode)
  {
    return io.open(mode);
  }
}

bool IoScopedState::changeState(QIODevice::OpenMode mode)
{
  bool willClosed = mode.testFlag(QIODevice::NotOpen);
  bool isClosed = io.openMode().testFlag(QIODevice::NotOpen);

  //io wanted to be closed, and it's closed -- return success
  if(willClosed && isClosed)
    return true;

  //io is opened, but wanted to be closed
  if(willClosed)
    {
      io.close();
      return !io.isOpen();
    } else
    //io is closed, but wanted to be opened
    if(isClosed)
      {
        return openIO(io, mode);
      } else
      {
        if(mode.testFlag(QIODevice::ReadOnly) > io.openMode().testFlag(QIODevice::ReadOnly)
           || mode.testFlag(QIODevice::WriteOnly) > io.openMode().testFlag(QIODevice::WriteOnly))
          return openIO(io, mode);


        //IO already opened with right permissions
        if(bool textMode = mode.testFlag(QIODevice::Text); textMode != io.openMode().testFlag(QIODevice::Text))
          io.setTextModeEnabled(textMode);

        return true;
      }

}

IoScopedState::IoScopedState(QIODevice &io, QIODevice::OpenMode state):
  io(io),
  lastState(io.openMode())
{
  if(!changeState(state))
    throw std::ios_base::failure("sib_utils::IoScopedState: can not change io state");
}

IoScopedState::~IoScopedState()
{
  changeState(lastState);
}
