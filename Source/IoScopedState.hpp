#ifndef IOSCOPEDSTATE_HPP
#define IOSCOPEDSTATE_HPP
#include <QIODevice>
#include "sib_utils_global.hpp"

namespace sib_utils {
  /**
   * @brief The IoScopedState class
   * Changes QIODevice::OpenMode, when constructed and restores mode, when deleted
   */
  class SIB_UTILS_SHARED_EXPORT IoScopedState
  {
    QIODevice &io;
    QIODevice::OpenMode lastState;

    bool changeState(QIODevice::OpenMode mode);

  public:
    IoScopedState(QIODevice &io, QIODevice::OpenMode state = QIODevice::ReadOnly | QIODevice::Text);

    ~IoScopedState();
  };
}

#endif // IOSCOPEDSTATE_HPP
