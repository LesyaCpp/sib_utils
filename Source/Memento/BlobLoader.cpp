#include "../IoScopedState.hpp"
#include "BlobLoader.hpp"
#include <QDataStream>
#include <QHash>
using namespace sib_utils::memento;

BlobLoader::BlobLoader()
{
}

namespace {
  QDataStream& operator >> (QDataStream &st, Memento *&memento)
  {
    struct LoadTask {
      QHash<QString, quint64> linkData;
      Memento *memento;
    };
    QVector<LoadTask> createdMemento;

    struct State {
      Memento *parentMemento;
    };

    QVector<State> stack = { State { nullptr } };

    while(!stack.isEmpty())
      {
        auto el = stack.takeLast();
        createdMemento.push_back(LoadTask());
        auto &x = createdMemento.back();
        x.linkData.clear();

        QString mementoType;
        QHash<QString, QString> txtData;
        QHash<QString, QByteArray> binData;

        st >> mementoType;

        if(el.parentMemento)
          el.parentMemento->mutator().createChildMemento(mementoType, x.memento);
        else
          memento = x.memento = new Memento(mementoType);

        st >> binData >> txtData >> x.linkData;
        auto memMutator = x.memento->mutator();

        for(auto i = binData.constBegin(); i != binData.constEnd(); ++i)
          memMutator.setData(i.key(), i.value());

        for(auto i = txtData.constBegin(); i != txtData.constEnd(); ++i)
          memMutator.setData(i.key(), i.value());

        quint64 childCount = 0,
            lastSz = stack.size();
        st >> childCount;
        stack.resize(lastSz + childCount);
        for(quint32 i = 0; i < childCount; ++i)
          stack[i + lastSz].parentMemento = x.memento;
      }

    for(auto i = createdMemento.begin(); i < createdMemento.end(); ++i)
      {
        auto memMutator = i->memento->mutator();
        for(auto j = i->linkData.constBegin(); j != i->linkData.constEnd(); ++j)
          memMutator.setRawLink(j.key(), createdMemento.at(j.value()).memento);
      }

    return st;
  }
}

Memento* BlobLoader::load(QIODevice &dev)
{
  IoScopedState state(dev, QIODevice::ReadOnly);
  QDataStream st(&dev);

  Memento *result = nullptr;
  st >> result;
  return result;
}

Memento *BlobLoader::load(const QByteArray &blob)
{
  QDataStream st(blob);

  Memento *result = nullptr;
  st >> result;
  return result;
}
