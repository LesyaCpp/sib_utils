#ifndef BLOBLOADER_HPP
#define BLOBLOADER_HPP

#include "Memento.hpp"
#include <QIODevice>

namespace sib_utils::memento {
  class SIB_UTILS_SHARED_EXPORT BlobLoader
  {
  public:
    BlobLoader();

    Memento* load(QIODevice &dev);

    inline Memento* load(QIODevice &&dev)
    { return load(static_cast<QIODevice&>(dev)); }

    Memento* load(const QByteArray &blob);
  };
}

#endif // BLOBLOADER_HPP
