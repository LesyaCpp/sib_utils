#include "../IoScopedState.hpp"
#include "BlobSaver.hpp"
#include <QtConcurrent>
#include <QDataStream>
#include <QHash>
#include <list>
using namespace sib_utils::memento;

BlobSaver::BlobSaver()
{
}

namespace {
  template<typename Key, typename Val>
  auto qhash_insert()
  { return qOverload<const Key&, const Val&>(&QHash<Key, Val>::insert); }

  QDataStream& operator << (QDataStream &st, Memento &memento)
  {
    QVector<Memento*> savingMemento;

    struct State {
      Memento *memento;
    };
    auto maxThreads = QThreadPool::globalInstance()->maxThreadCount();
    QThreadPool::globalInstance()->setMaxThreadCount(1);

    std::list<State> stack = { State {&memento} };
    while(!stack.empty())
      {
        auto el = stack.back();
        stack.pop_back();
        savingMemento.push_back(el.memento);
        el.memento->reader().forallChildrenR([&stack] (Memento &m) {
          stack.push_back( State { &m } );
        });
      }

    for(auto i = savingMemento.begin(); i != savingMemento.end(); ++i)
      {
        QString mementoType;
        QHash<QString, QByteArray> binData;
        QHash<QString, QString> txtData;
        QHash<QString, quint64> linkData;
        size_t chCount;

        auto reader = (*i)->reader();
        reader.getType(mementoType);
        reader.readAll(std::bind(qhash_insert<QString, QByteArray>(), std::ref(binData), std::placeholders::_1, std::placeholders::_2));
        reader.readAllText(std::bind(qhash_insert<QString,QString>(), std::ref(txtData), std::placeholders::_1, std::placeholders::_2));
        reader.readAllRawLinks([&linkData, &savingMemento] (const QString &key, Memento *value) {
            int index = savingMemento.indexOf(value);
            if(index >= 0)
              linkData.insert(key, index);
          });
        reader.getChildrenCount(chCount);

        st << mementoType << binData << txtData << linkData << static_cast<quint64>(chCount);
      }

    QThreadPool::globalInstance()->setMaxThreadCount(maxThreads);
    return st;
  }
}

void BlobSaver::save(QIODevice &dev, const Memento &mem)
{
  IoScopedState state(dev, QIODevice::WriteOnly);
  QDataStream st(&dev);
  st << const_cast<Memento&>(mem);
}

void BlobSaver::save(QByteArray &out, const Memento &mem)
{
  QDataStream st(&out, QIODevice::WriteOnly);
  st << const_cast<Memento&>(mem);
}
