#include "NamedQObjectIndex.hpp"
#include <algorithm>

sib_utils::NamedQObjectIndex::~NamedQObjectIndex()
{
  for(const auto &i: data)
    {
      QObject::disconnect(i.onDestroyed);
      QObject::disconnect(i.onRenamed);
    }
}

void sib_utils::NamedQObjectIndex::indexObject(QObject *obj)
{
  auto onDestroyed = QObject::connect(obj, &QObject::destroyed, [this](QObject *obj) { NamedQObjectIndex::unindexObject(obj); });
  auto onRenamed = QObject::connect(obj, &QObject::objectNameChanged,
                                      [this, obj] (const QString &newName) { onObjectNameChanged(obj, newName); });

  data.insert(obj->objectName(), DataElement { .obj = obj, .onDestroyed = onDestroyed, .onRenamed = onRenamed });

}

bool sib_utils::NamedQObjectIndex::unindexObject(QObject *obj)
{
  auto el = std::find_if(data.find(obj->objectName()), data.end(), [obj](DataElement &i) { return i.obj == obj; });
  if(el != data.end())
    {
      QObject::disconnect(el->onDestroyed);
      QObject::disconnect(el->onRenamed);
      data.erase(el);
      return true;
    }
  return false;
}

void sib_utils::NamedQObjectIndex::clear()
{
  for(auto &i: data)
    {
      QObject::disconnect(i.onDestroyed);
      QObject::disconnect(i.onRenamed);
    }
  data.clear();
}

void sib_utils::NamedQObjectIndex::foreachObject(const std::function<void(QObject*)> &func) const
{
  if(Q_LIKELY(func))
    for(const auto &i: data)
      func(i.obj);
}

QObject *sib_utils::NamedQObjectIndex::find(const QString &name) const
{
  auto el = data.find(name);
  if(el != data.end())
    return el->obj;
  else
    return nullptr;
}

void sib_utils::NamedQObjectIndex::foreachObject(const QString &name, const std::function<void (QObject *)> &func) const
{
  if(Q_UNLIKELY(!func)) return;

  auto el = data.find(name);
  while(el != data.end() && el.key() == name)
    {
      func(el->obj);
      ++el;
    }
}

QList<QObject*> sib_utils::NamedQObjectIndex::findAll(const QString &name) const
{
  QList<QObject*> result;
  auto el = data.find(name);
  while(el != data.end() && el.key() == name)
    {
      result.push_back(el->obj);
      ++el;
    }
  return result;
}

void sib_utils::NamedQObjectIndex::onObjectNameChanged(QObject *obj, const QString &newName)
{
  auto el = std::find_if(data.begin(), data.end(), [obj](DataElement &i) { return i.obj == obj; });
  if(Q_LIKELY(el != data.end()))
    {
      DataElement objData = *el;
      data.erase(el);
      data.insert(newName, objData);
    }
}
