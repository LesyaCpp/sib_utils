#ifndef NAMEDQOBJECTINDEX_HPP
#define NAMEDQOBJECTINDEX_HPP

#include "sib_utils_global.hpp"
#include <functional>
#include <QObject>
#include <QHash>

namespace sib_utils {

  /// Hash table of QObject instances. Key is QObject::objectName. The hash table tracks object name changes
  class SIB_UTILS_SHARED_EXPORT NamedQObjectIndex
  {
    NamedQObjectIndex(const NamedQObjectIndex&) = delete;
    NamedQObjectIndex& operator=(const NamedQObjectIndex&) = delete;

  public:
    NamedQObjectIndex() = default;
    ~NamedQObjectIndex();

    void indexObject(QObject *obj);
    bool unindexObject(QObject *obj);
    void clear();

    void foreachObject(const std::function<void(QObject*)> &func) const;

    QObject* find(const QString &name) const;
    void foreachObject(const QString &name, const std::function<void(QObject*)> &func) const;
    QList<QObject*> findAll(const QString &name) const;

  private:
    struct DataElement {
      QObject *obj;
      QMetaObject::Connection onDestroyed;
      QMetaObject::Connection onRenamed;
    };

    QMultiHash<QString, DataElement> data;

    void onObjectNameChanged(QObject *obj, const QString &name);
  };


  /// Hash table of the T instances. T should be based on QObject. Key is QObject::objectName. The hash table tracks object name changes
  template<typename T, class = std::enable_if_t<std::is_base_of_v<QObject, T>>>
  class NamedTypedQObjectIndex: private NamedQObjectIndex
  {
  public:
    NamedTypedQObjectIndex() = default;
    ~NamedTypedQObjectIndex() = default;

    void indexObject(T *obj)
    {
      NamedQObjectIndex::indexObject(obj);
    }

    bool unindexObject(T *obj)
    {
      return NamedQObjectIndex::unindexObject(obj);
    }

    using NamedQObjectIndex::clear;


    void foreachObject(const std::function<void(T*)> &func) const
    {
      if(Q_LIKELY(func))
        NamedQObjectIndex::foreachObject([&func](T *obj) { func(static_cast<T*>(obj)); });
    }


    T* find(const QString &name) const
    {
      return static_cast<T*>(NamedQObjectIndex::find(name));
    }

    void foreachObject(const QString &name, const std::function<void(T*)> &func) const
    {
      if(Q_LIKELY(func))
        NamedQObjectIndex::foreachObject(name, [&func](T *obj) { func(static_cast<T*>(obj)); });
    }
  };
}

#endif // NAMEDQOBJECTINDEX_HPP
