#ifndef QTTOSTDHASH_HPP
#define QTTOSTDHASH_HPP

#include <QHash>

namespace sib_utils {
  template<typename T>
  struct QtHashFunc
  {
    uint operator()(T const &val) const noexcept
    {
      return qHash(val);
    }
  };
}

#endif // QTTOSTDHASH_HPP
