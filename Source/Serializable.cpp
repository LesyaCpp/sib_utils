#include "Serializable.hpp"

namespace {
  QDataStream& operator<<(QDataStream &st, const std::string_view &str)
  {
    st << static_cast<quint64>(str.size());
    for(char c: str)
      st << static_cast<quint8>(c);
    return st;
  }
}

sib_utils::Serializable::Serializable(const std::string_view &adapterId, std::unordered_map<std::string_view, ByteAdapter> &adapters):
  adapterId(adapterId),
  adapters(adapters)
{
}

void sib_utils::Serializable::saveTo(QDataStream &st) const
{
  auto el = adapters.find(adapterId);
  if(Q_UNLIKELY(el == adapters.end()))
    throw std::runtime_error("sib_utils::Serializable::saveTo: invalid adapterId");

  st << adapterId;
  el->second.saver(st, *this);
}

void sib_utils::Serializable::stubSaveTo(QDataStream &st)
{
  st << quint64(0);
}

std::unique_ptr<sib_utils::Serializable> sib_utils::Serializable::loadFrom(QDataStream &st, const std::unordered_map<std::string_view, ByteAdapter> &adapters)
{
  quint64 strSize;
  st >> strSize;
  std::string str(strSize, ' ');
  for(char &i: str)
    {
      quint8 ch;
      st >> ch;
      i = static_cast<char>(ch);
    }
  auto el = adapters.find(std::string_view(str.data(), str.size()));
  if(el == adapters.end())
    return std::unique_ptr<sib_utils::Serializable>();

  auto result = el->second.creator();
  el->second.loader(st, *result);
  return result;
}
