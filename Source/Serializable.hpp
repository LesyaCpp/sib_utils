#ifndef SERIALIZABLE_HPP
#define SERIALIZABLE_HPP

#include "sib_utils_global.hpp"

#include <QDataStream>
#include <string_view>
#include <functional>
#include <memory>

namespace sib_utils {

  /// This is a base class for serializing and deserializing any polymorphic class. Implements saveTo(QDataStream) and static loadFrom(QDataStream).
  class SIB_UTILS_SHARED_EXPORT Serializable
  {
  protected:
    struct ByteAdapter
    {
      std::function<std::unique_ptr<Serializable>()> creator;
      std::function<void (QDataStream&, const Serializable &obj)> saver;
      std::function<void (QDataStream&, Serializable &obj)> loader;
    };


    Serializable(const std::string_view &adapterId, std::unordered_map<std::string_view, ByteAdapter> &adapters);

  public:
    virtual ~Serializable() = default;

    /// Serializes *this to the data stream
    void saveTo(QDataStream &st) const;

    inline std::string_view getSerializableType() const
    { return adapterId; }

    /// Serializes a null object to the data stream
    static void stubSaveTo(QDataStream &st);



    // IMPLEMENT
  protected:
    /// Creates a new object from the data stream
    static std::unique_ptr<Serializable> loadFrom(QDataStream &st, const std::unordered_map<std::string_view, ByteAdapter> &adapters);

    template<typename T, std::unordered_map<std::string_view, ByteAdapter> &adapters>
    class AdapterRegistrator
    {
    public:
      std::string_view adapterId;

      inline AdapterRegistrator(const std::string_view &adapterId):
        adapterId(adapterId)
      {
        auto adapter = adapters.emplace(std::piecewise_construct,
                                        std::forward_as_tuple(adapterId),
                                        std::tuple()).first;
        adapter->second.creator = []() { return std::make_unique<T>(); };
        adapter->second.loader = [](QDataStream &st, Serializable &obj) { st >> dynamic_cast<T&>(obj); };
        adapter->second.saver = [](QDataStream &st, const Serializable &obj) { st << dynamic_cast<const T&>(obj); };
      }

      inline ~AdapterRegistrator()
      {
        auto el = adapters.find(adapterId);
        if(el != adapters.end())
          adapters.erase(el);
      }
    };


    const std::string_view adapterId;
    std::unordered_map<std::string_view, ByteAdapter> &adapters;
  };
}

#endif // SERIALIZABLE_HPP
