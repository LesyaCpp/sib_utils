#include "Strings.hpp"

QString sib_utils::strings::getQStr(const std::string_view &str)
{
  QString result(str.size(), QChar());
  for(int i = 0; i < static_cast<int>(str.size()); ++i)
    result[i] = str[i];
  return result;
}
