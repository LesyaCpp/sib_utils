#include "TreeNode.hpp"
#include <QChildEvent>
#include <QVariant>
#include <QThread>
#include <list>

using namespace sib_utils;
using memento::Memento;

TreeNode::TreeNode()
{
}

void TreeNode::saveBinary(Memento::DataMutator &memento) const
{
  memento.setData("ObjectName", objectName());
  writeBinary(memento);
}

void TreeNode::saveText(Memento::DataMutator &memento) const
{
  memento.setData("ObjectName", objectName());
  writeText(memento);
}

void TreeNode::load(Memento::DataReader &memento)
{
  memento.readText("ObjectName", [this](const QString &objName){ setObjectName(objName); });
  read(memento);
}

void TreeNode::updateParent()
{
  std::list<TreeNode*> affectedNodes,
      processingNodes = { this };

  while(!processingNodes.empty())
    {
      auto node = processingNodes.back();
      processingNodes.pop_back();
      affectedNodes.push_back(node);

      for(auto ch = node->children().rbegin(); ch != node->children().rend(); ++ch)
        if(auto childNode = dynamic_cast<TreeNode*>(*ch); childNode)
          processingNodes.push_back(childNode);
    }

  for(TreeNode *node: affectedNodes)
    emit node->parentUpdated(nullptr);
}

void TreeNode::updateParent(TreeNode *parent)
{
  if(parent && thread() != parent->thread() && thread() == QThread::currentThread())
    moveToThread(parent->thread());
  setParent(parent);
  updateParent();
}

QString TreeNode::getMementoType() const
{
  return property("MementoType").toString();
}

void TreeNode::childEvent(QChildEvent *event)
{
  switch(event->type())
    {
    case QEvent::ChildAdded:
      if(auto node = dynamic_cast<TreeNode*>(event->child()); node)
        {
          node->updateParent();
          emit childAdded(node);
        }
      break;

    case QEvent::ChildRemoved:
      if(auto node = dynamic_cast<TreeNode*>(event->child()); node)
        {
          node->updateParent();
          emit childRemoved(node);
        }
      break;

    default: break;
    }
}
