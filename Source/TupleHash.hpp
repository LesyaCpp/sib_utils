#ifndef TUPLEHASH_HPP
#define TUPLEHASH_HPP

#include <functional>
#include <utility>

namespace sib_utils {
  template<typename... T>
  class TupleHash
  {
    template<typename... ArgT, size_t... ind>
    std::size_t hash(std::tuple<ArgT...> const &val, const std::index_sequence<ind...> &) const noexcept
    {
      return ((std::hash<ArgT>{}(std::get<ind>(val)) << ind) ^ ...);
    }

  public:
    std::size_t operator()(std::tuple<T...> const &val) const noexcept
    {
      return hash(val, std::make_index_sequence<std::tuple_size_v<std::tuple<T...>>>());
    }
  };

  template<typename... T>
  class TupleHashCustom
  {
    template<typename... ArgT, size_t... ind>
    std::size_t hash(std::tuple<ArgT...> const &val, const std::index_sequence<ind...> &) const noexcept
    {
      return ((T{}(std::get<ind>(val)) << ind) ^ ...);
    }

  public:
    template<typename... ArgT>
    std::size_t operator()(std::tuple<ArgT...> const &val) const noexcept
    {
      return hash(val, std::make_index_sequence<std::tuple_size_v<std::tuple<ArgT...>>>());
    }
  };
}

#endif // TUPLEHASH_HPP
