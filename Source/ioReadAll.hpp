#ifndef IOREADALL_HPP
#define IOREADALL_HPP

#include "sib_utils_global.hpp"
#include <QIODevice>
#include <QByteArray>

namespace sib_utils {
  SIB_UTILS_SHARED_EXPORT QByteArray ioReadAll(QIODevice &io, QIODevice::OpenMode state = QIODevice::ReadOnly | QIODevice::Text);

  SIB_UTILS_SHARED_EXPORT inline QByteArray ioReadAll(QIODevice &&io, QIODevice::OpenMode state = QIODevice::ReadOnly | QIODevice::Text)
  {
    return ioReadAll(static_cast<QIODevice&>(io), state);
  }
}

#endif // IOREADALL_HPP
