#ifndef JSONLOADERS_HPP
#define JSONLOADERS_HPP

#include "sib_utils_global.hpp"

#include <QJsonValue>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

namespace sib_utils::json {
  SIB_UTILS_SHARED_EXPORT glm::vec2 readVec2(const QJsonValue &val, const glm::vec2 &defaultVal);
  SIB_UTILS_SHARED_EXPORT glm::vec3 readVec3(const QJsonValue &val, const glm::vec3 &defaultVal);
  SIB_UTILS_SHARED_EXPORT glm::vec4 readVec4(const QJsonValue &val, const glm::vec4 &defaultVal);
  SIB_UTILS_SHARED_EXPORT glm::mat4 readMat4(const QJsonValue &val, const glm::mat4 &defaultVal);
}

#endif // JSONLOADERS_HPP
