#ifndef LOWESTHIGHEST_HPP
#define LOWESTHIGHEST_HPP

#include <initializer_list>
#include <utility>
#include <limits>

namespace sib_utils
{
  template<typename T>
  T getLowest(const std::initializer_list<T> &arg)
  {
    T r = std::numeric_limits<T>::max();
    for(const auto &i: arg)
      if(i < r)
        r = i;
    return r;
  }

  template<typename T>
  T getHighest(const std::initializer_list<T> &arg)
  {
    T r = std::numeric_limits<T>::lowest();
    for(const auto &i: arg)
      if(i > r)
        r = i;
    return r;
  }

  template<typename T>
  constexpr T getLowest(T t)
  {
    return t;
  }

  template<typename T, typename... Tail>
  constexpr T getLowest(T t, Tail&&... tail)
  {
    const auto lowest_tail = getLowest(std::forward<Tail>(tail)...);
    if(t < lowest_tail)
      return t;
    else
      return lowest_tail;
  }

  static_assert(getLowest(1,2,3,4,5) == 1);
  static_assert(getLowest(5,4,3,2,1) == 1);

  template<typename T>
  constexpr T getHighest(T t)
  {
    return t;
  }

  template<typename T, typename... Tail>
  constexpr T getHighest(T t, Tail&&... tail)
  {
    const auto highest_tail = getHighest(std::forward<Tail>(tail)...);
    if(t > highest_tail)
      return t;
    else
      return highest_tail;
  }

  static_assert(getHighest(1,2,3,4,5) == 5);
  static_assert(getHighest(5,4,3,2,1) == 5);
}

#endif // LOWESTHIGHEST_HPP
