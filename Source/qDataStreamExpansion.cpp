#include "qDataStreamExpansion.hpp"

QDataStream &operator<<(QDataStream &st, std::byte a)
{
  return st << static_cast<quint8>(a);
}

QDataStream &operator>>(QDataStream &st, std::byte &a)
{
  quint8 val;
  st >> val;
  a = static_cast<std::byte>(val);
  return st;
}

template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<quint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<qint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<float> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::optional<double> &a);

template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<quint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<qint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<float> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::optional<double> &a);

template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<quint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<qint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<float> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::vector<double> &a);

template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<quint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<qint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<float> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::vector<double> &a);


template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<quint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<qint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<float> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator>>(QDataStream &st, std::list<double> &a);

template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<quint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint8> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint16> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint32> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<qint64> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<float> &a);
template SIB_UTILS_SHARED_EXPORT QDataStream &operator<<(QDataStream &st, const std::list<double> &a);
