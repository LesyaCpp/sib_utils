#ifndef SIB_UTILS_GLOBAL_HPP
#define SIB_UTILS_GLOBAL_HPP
#include <qglobal.h>

#if defined(SIB_UTILS_LIBRARY)
#  define SIB_UTILS_SHARED_EXPORT Q_DECL_EXPORT
#else
#  define SIB_UTILS_SHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SIB_UTILS_GLOBAL_HPP
